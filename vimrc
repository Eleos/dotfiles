" Author: Eleos
" vimrc from bunch of stolen stuff here and there.

" IMproved
set nocompatible
set runtimepath+=~/.vim/
set mouse=a

" Neobundle initialization.
set runtimepath+=~/.vim/bundle/neobundle.vim/
call neobundle#begin(expand('~/.vim/bundle/'))
NeoBundleFetch 'Shougo/neobundle.vim'

" Plugins
NeoBundle 'editorconfig/editorconfig-vim'
NeoBundle 'sjl/gundo.vim'
NeoBundle 'mhinz/vim-startify'
NeoBundle 'majutsushi/tagbar'
NeoBundle 'vim-airline/vim-airline'
NeoBundle 'vim-airline/vim-airline-themes'
NeoBundle 'tpope/vim-fugitive'
NeoBundle 'rust-lang/rust.vim'
NeoBundle 'vim-syntastic/syntastic'
NeoBundle 'vim-pandoc/vim-pandoc'
NeoBundle 'vim-pandoc/vim-pandoc-syntax'
NeoBundleCheck
call neobundle#end()

" General options
set background=dark
set foldmethod=indent
set hlsearch
set ignorecase
set incsearch
set laststatus=2
set listchars=tab:>-,trail:�,extends:>,precedes:<,nbsp:�
set list
set modeline
set noesckeys
set number
set scrolloff=10
set sidescrolloff=5
set showcmd
set smartcase
set swapsync=""
set updatetime=4000
set tw=80
set wildmenu
set wildmode=longest,list:longest,list:full
set spelllang=fr,en
set syntax on

" Tabs and spaces
set autoindent
set expandtab
set tabstop=4
set softtabstop=4
set shiftwidth=4

" Keyboard remamps
noremap <Space> :
nmap ! :!
nmap <Leader>/ :set hlsearch!
nmap <Leader>t :NERDTree<CR>
nmap <Leader>s :SyntasticToggleMode<CR>
cmap w!! w !sudo tee % >/dev/null

" Buffer splits exploration
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" Previous / next buffer
map <Leader>n :bn<cr>
map <Leader>p :bp<cr>
map <Leader>d :bd<cr>

" Map to copy to clipboard (still experiemental)
" Copy to X CLIPBOARD
map <leader>cc :w !xsel -i -b<CR>
map <leader>cp :w !xsel -i -p<CR>
map <leader>cs :w !xsel -i -s<CR>
" " Paste from X CLIPBOARD
map <leader>pp :r!xsel -p<CR>
map <leader>ps :r!xsel -s<CR>
map <leader>pb :r!xsel -b<CR>


" Vertical split style
set fillchars+=vert:\ 

" Aliases
cnoreabbrev W w

" Special
filetype plugin indent on
syn on

" Airline
let g:airline_powerline_fonts = 0
let g:airline_theme = 'lucius'
let g:airline#extensions#tabline#enabled = 1

" Clear problem with tmux and colorscheme
colorscheme ron

" Fold color
hi Folded ctermfg=Green
hi Folded ctermbg=Black

" Syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_python_checker = ['flake8']
let g:syntastic_mode_map = {'mode': 'passive', 'active_filetypes': [],'passive_filetypes': []}
let g:syntastic_quiet_messages = { "type": "style" }
