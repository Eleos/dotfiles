# dotfiles

Various dotfiles of my system.

There are configs for:
 * i3
 * a custom i3exit (might be moved soon)
 * ranger
 * tmux
 * vimrc
 * Xressources
 * zshrc

Those are mostly modification of various dotfiles I found on the vast hills of internet, use them at your own risk (I don't think they can break your computer but better safe than sorry).
