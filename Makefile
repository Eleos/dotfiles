HOME_FOLDER=/home/skinnery/tmpf/test

all: help

help:
	@echo 'Makefile to create links for dotfiles'
	@echo ''
	@echo 'Usage:'
	@echo '   make i3: link i3 confing and i3exit'
	@echo '   make tmux: link tmux config'
	@echo '   make fish: link fish config'
	@echo '   make serv_zsh: link serv zshrc'
	@echo '   make user_zsh: link user zshrc (need oh-my-zsh)'
	@echo '   make ranger: link ranger config'
	@echo '   make vimrc: link (heavy) vimrc conf'
	@echo '   make Xresources: link Xressources'
	@echo '   make Xresources: link xinitrc (Not recommanded)'

install:

i3:
	./config_dotfiles.sh i3

tmux:
	./config_dotfiles.sh tmux

fish:
	./config_dotfiles.sh fish

ranger:
	./config_dotfiles.sh ranger

rofi:
	./config_dotfiles.sh rofi

vim:
	./config_dotfiles.sh vim

xinitrc:
	./config_dotfiles.sh xinitrc

Xresources:
	./config_dotfiles.sh Xresources

user_zsh:
	./config_dotfiles.sh Xresources

server_zsh:
	./config_dotfiles.sh server_zsh

install: i3 tmux fish ranger rofi vim Xresources

serv_install: tmux server_zsh
