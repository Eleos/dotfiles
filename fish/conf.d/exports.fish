set -x PATH $PATH "$HOME/prog/script_shell/custom_scripts"
set -x PATH $PATH "$HOME/prog/script_shell/priv_scripts"
set -x PATH $PATH "$HOME/.local/bin"
set -x PATH "$HOME/.cargo/bin" $PATH 

set -x EDITOR vim
set -x XDG_CONFIG_HOME "$HOME/.config"
