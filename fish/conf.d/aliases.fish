# Aliases
# pip
function pip
    python -m pip $argv
end

# todo and journal aliases
function todo
    cat -n "$HOME/todolist/TODO"
end

function etodo
    vim "$HOME/todolist/TODO"
end

function today
    sh "$HOME/journal/today.sh"
end

# dnf aliases
function update
    sudo pacman -Syyu
end

function search
    pacman -Ss $argv
end

function install
    sudo pacman -Sy $argv
end

# Dir/Ranger aliases
function books
    ranger "$HOME/Documents/books"
end
function uni
    ranger "$HOME/Nextcloud/Documents/r1/l3_s5"
end
function jdr
    ranger "$HOME/Documents/JDR"
end
function trash
    ranger "$HOME/.local/share/Trash/files"
end

# Program replacements
function ls
    exa $argv
end
function lsl
    ls -l $argv
end
function cat
    bat $argv
end
function calm
    cal -m $argv
end

# Exits
function :wq
    exit
end
function :q
    exit
end
function :x
    exit
end

# Others
function swapcapsesc
    setxkbmap -option caps:swapescape
end
function mpdmop
    ncmpcpp -p 6601
end
function colortest
    msgcat --color=test
end
