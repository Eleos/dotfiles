# Author : Eleos
# (mostly small modification and addings from auto generated
# zshrc file)

# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
  export ZSH="$HOME/.oh-my-zsh"

# Path to private variables
  source "$HOME/.private_zsh"

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="fino"

# Set list of themes to load
# Setting this variable when ZSH_THEME=random
# cause zsh load theme from this variable instead of
# looking in ~/.oh-my-zsh/themes/
# An empty array have no effect
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
  git vi-mode zsh-syntax-highlighting
)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.

# Aliases

# TODO: Fix this (FLEEEEEEEEEEMME)
# Alias for pip
alias pip="python -m pip"

# Vim aliases
alias ohmyzsh="vim $HOME/.oh-my-zsh"
alias zshconfig="vim $HOME/.zshrc"
alias todo="cat $HOME/todolist/TODO.md"
alias etodo="vim $HOME/todolist/TODO.md"
alias today="sh $HOME/journal/today.sh"

# Quick xclip
alias selfquest="xclip -i $HOME/Documents/Perso/question_selfcare/questions.md"

# dnf aliases
alias update='sudo dnf upgrade'
alias install='sudo dnf install'

# Dir/Ranger aliases
alias books="ranger $HOME/Documents/books"
alias uni="ranger $HOME/Nextcloud/Documents/r1/l3_s5"
alias jdr="ranger $HOME/Documents/JDR"
alias trash="ranger $HOME/.local/share/Trash/files"

# Replacement aliases
alias ls='exa'
alias lsl='ls -l'
alias cat='bat'

# Exit
alias :wq='exit'
alias :x='exit'
alias :q='exit'

# Other aliases
alias swapcapsesc='setxkbmap -option caps:swapescape'
# alias mopspotify="nohup mopidy &;mkfifo /tmp/mpd.fifo;while :; do yes $’\n’ | nc -lu 127.0.0.1 5555 > /tmp/mpd.fifo; done &;"
alias mpdmop="ncmpcpp -p 6601"
alias colortest="msgcat --color=test"

# Program who needs some flags
alias gdb='gdb -tui'
alias diff='diff --color'
alias cal='cal -m'

# PATH export extensions
export PATH="$HOME/prog/script_shell/custom_scripts:$PATH"
export PATH="$HOME/prog/script_shell/priv_scripts:$PATH"
export PATH="$HOME/.local/bin:$PATH"
export PATH="$HOME/.local/opt/sbt/bin:$PATH"

# Export LIB_PATH cause game SUCKS loading libs
# export LD_LIBRARY_PATH="$HOME/.local/share/Steam/ubuntu12_32/steam-runtime/usr/lib/x86_64-linux-gnu:$LD_LIBRARY_PATH"

# Other exports
export EDITOR=vim
export XDG_CONFIG_HOME="$HOME/.config"

# Python exports
# export WORKON_HOME="$HOME/.virtualenvs"
# export PROJECT_HOME="$HOME/prog/python/Devel"
# source "$HOME/.local/bin/virtualenvwrapper.sh"
# export VIRTUALENVWRAPPER_PYTHON="/usr/bin/python"

# Python LearnTDD (TODO: TO DELETE ONCE DONE)
# export PATH="$HOME/prog/python/learn_tdd:$PATH"

# Vim mode
# bindkey -v
export KEYTIMEOUT=1
MODE_INDICATOR="%{$FX[italic]$FG[001]%}NORMAL%{$FX[reset]%}"
