#!/bin/bash

dmln () {
    if [[ -z "$1" || -z "$2" ]]; then
        echo "format dlmn dotfile_path file_path";
        return 1;
    fi

    if [[ ! ( -f "$1" || -d "$1" ) ]]; then
        echo "$1 isn't a valid file or folder";
        return 1;
    fi

    if [[ -f "$2" || -d "$2" ]]; then
        echo "$2 already exists, exiting"
        return 1;
    fi

    ln -s "$1" "$2"
    return 0
}

if [[ -z "$1" ]]; then
    echo "No parameter were given, exiting"
    exit 1
fi

pwdvar=$(pwd)
case "$1" in
    "i3")
        dmln "$pwdvar/i3" "$HOME/.config/i3"
    ;;
    "tmux")
        dmln "$pwdvar/tmux.conf" "$HOME/.tmux.conf"
    ;;
    "fish")
        dmln "$pwdvar/fish" "$HOME/.config/fish"
    ;;
    "ranger")
        dmln "$pwdvar/ranger" "$HOME/.config/ranger"
    ;;
    "rofi")
        echo "Not implemented yet"
    ;;
    "server_zsh")
        dmln "$pwdvar/server_zshrc" "$HOME/.zshrc"
    ;;
    "vim")
        dmln "$pwdvar/vimrc" "$HOME/.vimrc"
    ;;
    "xinitrc")
        dmln "$pwdvar/xinitrc" "$HOME/.xinitrc"
    ;;
    "Xresources")
        dmln "$pwdvar/.Xresources" "$HOME/.Xresources"
    ;;
    "user_zsh")
        dmln "$pwdvar/server_zshrc" "$HOME/.zshrc"
    ;;
    *) echo "$1 isn't implimented"
    ;;
esac

exit 0
